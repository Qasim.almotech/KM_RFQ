<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use App\Models\AnsCategory;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {





        // user
        Permission::create(['name' => 'user_add']);
        Permission::create(['name' => 'user_show']);
        Permission::create(['name' => 'user_delete']);
        Permission::create(['name' => 'user_edit']);

        // role
        Permission::create(['name' => 'role_add']);
        Permission::create(['name' => 'role_show']);
        Permission::create(['name' => 'role_delete']);
        Permission::create(['name' => 'role_edit']);
        // report
        Permission::create(['name' => 'report_get']);
        AnsCategory::create(['name' => 'ans01', 'description' => 'تدارکات اجناس و خدمات غیر مشورتی']);
        AnsCategory::create(['name' => 'ans02', 'description' => 'تدارکات امور ساختمانی']);
        AnsCategory::create(['name' => 'ans03', 'description' => 'تدارکات امور اجاره دهی و خدمات مشورتی']);
        AnsCategory::create(['name' => 'ans04', 'description' => 'اطلاعیه‌های تصمیم اعطای قرارداد']);
        AnsCategory::create(['name' => 'ans05', 'description' => 'لیست قرار داد های اعطاء شده']);

        $user = User::create([
            'name' => 'Habib',
            'last_name' => 'Khan',
            'position' => 'MIS',
            'contact_number' => '0773040738',
            'email' => 'habib@km.gov.af',
            'password' => Hash::make('habib@km.gov.af'),
        ]);

        $role = Role::create(['name' => 'ادمین']);

        $role->syncPermissions(Permission::all());

        $user->syncRoles($role);


    }
}
