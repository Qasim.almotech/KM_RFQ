<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\AnnounceController;
use App\Http\Controllers\CompanyController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();
Route::get('/', [HomeController::class, 'front'])->name('front');
Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
//Route::get('announce_add/save', [AnnounceController::class, 'saveRecord'])->name('announce_add/save');

Route::resource('role', RoleController::class);
Route::resource('user', UserController::class);
Route::resource('announce', AnnounceController::class);
// Route::resource('announce_list', AnnounceController::class);
//Route::resource('announce_add', AnnounceController::class);

Route::resource('company', CompanyController::class);
Route::get('reports',[HomeController::class, 'reportForm'])->name('report.form');
Route::get('rfq/report',[HomeController::class, 'rfqReport'])->name('rfq.report');
Route::get('company/list/{id}',[HomeController::class, 'companyList'])->name('company.list');

Route::view('/test', 'layouts.main');