<?php

return [

    //
    'apartment_add' => 'ثبت آپارتمان',
    'apartment_show' => 'لیست آپارتمان',
    'apartment_delete' => 'حذف آپارتمان',
    'apartment_edit' => 'اصلاح آپارتمان',
    //
    'shahrak_add' => 'ثبت شهرک',
    'shahrak_show' => 'لیست شهرک',
    'shahrak_delete' => 'حذف شهرک',
    'shahrak_edit' => 'اصلاح شهرک',
    //
    'land_add' => 'ثبت زمین',
    'land_show' => 'لیست زمین',
    'land_delete' => 'حذف زمین',
    'land_edit' => 'اصلاح زمین',
    //
    'payment_add' => 'ثبت پرداخت',
    'payment_show' => 'لیست پرداخت',
    'payment_delete' => 'حذف پرداخت',
    'payment_edit' => 'اصلاح پرداخت',
    //
    'resident_add' => 'ثبت خریدار',
    'resident_show' => 'لیست خریدار',
    'resident_delete' => 'حذف خریدار',
    'resident_edit' => 'اصلاح خریدار',
    //
    'document_add' => 'ثبت سند',
    'document_show' => 'لیست اسناد',
    'document_delete' => 'حذف سند',
    'document_edit' => 'اصلاح سند',
    //
    'user_add' => 'ثبت کاربر',
    'user_show' => 'لیست کاربر',
    'user_delete' => 'حذف کاربر',
    'user_edit' => 'اصلاح کاربر',
    //
    'role_add' => 'ثبت حق دسترسی',
    'role_show' => 'لیست حق دسترسی',
    'role_delete' => 'حذف حق دسترسی',
    'role_edit' => 'اصلاح حق دسترسی',




];
