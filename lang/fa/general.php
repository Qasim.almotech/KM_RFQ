<?php
return [

    'kabul_municipality' => 'شاروالی کابل',
    'shahrak_list' => 'لیست شهرک ها',
    'shahraks' => 'شهرک ها',
    'add_shahrak' => 'ثبت شهرک',
    'residents' => 'خریداران',
    'resident_list' => 'لیست خریداران',
    'add_resident' => 'ثبت خریدار',
    'land_list' => 'لیست زمین ها',
    'lands' => 'زمین ها',
    'add_land' => 'ثبت زمین',
    'apartments' => 'آپارتمان ها',
    'add_apartment' => 'ثبت آپارتمان',
    'payments' => 'پرداخت ها',
    'list_payment' => 'لیست پرداخت',
    'add_payment' => 'ثبت پرداخت',
    'add_document' => 'ثبت سند',
    'dashboard' => 'داشبورد',
    'shahrak_name' => 'نام شهرک',
    'no_land_ap' => 'تعداد زمین',
    'district' => 'ناحیه/ولسوالی',
    'owner_name' => 'اسم مالک',
    'contact_number' => 'نمبر تماس',
    'options' => 'گزینه ها',
    'view' => 'نمایش',
    'edit' => 'اصلاح',
    'delete' => 'حذف',
    'total_area_size' => 'مساحت کلی',
    'NIC' => 'تذکره نمبر',
    'longtitude' => 'طول‌البلد',
    'lititude' => 'عرض‌ البلد',
    'father_name' => 'اسم پدر',
    'grandfather_name' => 'اسم پدر کلان',
    'description' => 'توضیحات',
    'm2' => 'متر مربع',
    'name' => 'اسم',
    'edit_resident' => 'ویرایش خریدار',
    'resident_name' => 'اسم خریدار',
    'owner' => 'مالک',
    'shahrak' => 'شهرک',
    'land_no' => 'نمبر زمین',
    'area_size' => 'مساحت',
    'persentage' => 'فیصدی',
    'net_amount' => 'سهم فیصدی',
    'government' => 'دولتی',
    'private' => 'شخصی',
    'ap_no' => 'نمبر آپارتمان',
    'edit_land' => 'ویرایش زمین',
    'tip' => 'تیپ',
    'edit_apartment' => 'ویرایش آپارتمان',
    'record_deleted' => ' ریکارد موفقانه حذف شد.',
    'apartment_name' => 'نام آپارتمان',
    'land_name' => 'نام زمین',
    'bill_no' => 'نمبر آویز',
    'partial_no' => 'نمبرقسط',
    'amount' => 'مقدار پرداخت',
    'date' => 'تاریخ',
    'payment_date' => 'تاریخ پرداخت',
    'is_apartment' => 'آپارتمان',
    'is_land' => 'زمین',
    'is_shahrak' => 'شهرک',
    'edit_shahrak' => 'ویرایش شهرک',
    'footer_copyright' => 'تمام حقوق مادی و معنوی سیستم موبوط شاروالی کابل میباشد.',
    'users' => 'کاربران',
    'add_user' => 'ثبت کاربر',
    'list_users' => 'لیست کاربران',
    'documents' => 'اسناد',
    'list_docs' => 'لیست اسناد',
    'add_docs' => 'ثبت سند',
    'logout' => 'خارج شدن',
    'province' => 'ولایت',
    'village' => 'قریه/گذر',
    'book_no' => 'جلد نمبر ',
    'page_no' => 'صفحه نمبر ',
    'east' => 'شرق',
    'west' => 'غرب',
    'north' => 'شمال',
    'south' => 'جنوب',
    'cost' => 'قیمت',
    'block_no' => 'نمبر بلاک',
    'hall_no' => 'نمبر دهلیز',
    'rooms' => 'تعداد اطاق',
    'bathrooms' => 'تعداد تشناب',
    'kitchen' => 'تعداد آشپزخانه',
    'floor_no' => 'منزل',

    'last_name' => 'تخلص',
    'email' => 'ایمیل',
    'password' => 'رمز عبور',
    'confirm_password' => 'تکرار رمز عبور',
    'app_name' => 'سیستم اعلان داوطلبی',
    'login_title' => 'به سیستم وارد شوید',
    'remember_me' => 'مرا به خاطر بسپار.',
    'login' => 'ورود',
    'forgot_your_password' => 'رمز عبور را فراموش کردید؟',
    'role' => 'صلاحیت',
    'position' => 'عنوان بست',
    'role_name' => 'نام حق‌دسترسی',
    'add_role' => 'ثبت حق‌دسترسی',
    'roles' => 'حق دسترسی',
    'permissions' => 'صلاحیت‌ها',
    'update_role' => 'ثبت صلاحیت ها',
    'title' => 'عنوان',
    'user_list' => 'لیست کاربران',
    'edit_user' => 'اصلاح کاربر',

    'list_document' => 'لیست اسناد',
    'type_docs' => 'نوع سند',


    'select_district' => 'انتخاب ناحیه ...',
    'property_type' => 'نوع نمره',
    'property_status' => 'وضعیت نمره',
    'payment_status' => 'وضعیت پرداخت',
    'paid' => 'پرداخت شده',
    'due' => 'باقی داری',
    'start_date' => 'تاریخ شروع',
    'end_date' => 'تاریخ ختم',
    'get_report' => 'گرفتن گزارش',
    'reports' => 'گزارشات',
    'select_shahrak' => 'انتخاب شهرک ...',
    'total_price' => 'قیمت مجموعی',
    'land_apt_no' => 'نمبر زمین/آپارتمان',
    'main_address' => 'سکونت اصلی',

    'contract_company' => 'شرکت قرارداد کننده',
    'contract_no' => 'نمبر قرارداد',
    'contract_date' => 'تاریخ قرارداد',
    'sentence_no' => 'نمبر حکم',
    'contract_department' => 'ریاست قرارداد کننده',
    'monitoring' => 'نظارت کننده',
    'contract_percentage' => 'سهم فیصدی مطابق قرارداد',
    'duration_date' => 'شروع و ختم تحویلی',
    'contract_information' => 'معلومات قرارداد',
    'plan_information' => 'معلومات پلان',
    'shahrak_information' => 'معلومات شهرک',
    'mutch_land' => 'تعداد نمرات',
    'mutch_blocks' => 'تعداد بلاک ها',
    'other_area_size' => 'مساحت عام المنفعه و متفرقه',

    'description_plan' => 'تفصیلات برویت پلان',
    'numbers_by_plan' => 'تعداد برویت پلان',
    'published_apt_land' => 'نمرات/بلاک توزیع شده',
    'balance' => 'باقی مانده',
    'paid_afg' => 'مبلغ تحویل شده به افغانی',
    'comment' => 'ملاحظات',
    'total_paid' => 'مجموع مبلغ تحویلی',
    'pendding_payment' => 'حالت جاری تحویلی',
    'total' => 'مجموع',
    'show_shahrak' => 'معلومات شهرک',
    'resident_show' => 'اطلاعات خریدار',
    'apartment_chart' => 'گراف آپارتمان',
    'land_chart' => 'گراف زمین',
    'more_info' => 'اطلاعات بیشتر',
    'afn' => 'افغانی',
    'resident_info' => 'معلومات خریدار',
    'resident_property' => 'املاک خریدار',
    'land_info' => 'معلومات زمین',
    'apartment_info' => 'معلومات آپارتمان',
    'four_sides' => 'چهار اطراف',
    'details' => 'جزیات',
    'choose_file' => 'انتخاب فایل',

    'page_not_found' => 'صفحه مورد نظر پیدا نشد',
    'you_do_not_have_permission' => 'شما حق دسترسی به این صفحه را ندارید',
    'return' => 'برگشت',
    'to_dashboard' => 'به داشبورد',
    '404' => '۴۰۴',
    'select' => 'انتخاب ...',

    'apartment_government' => ' اپارتمان دولتی ',
    'apartment_private' => 'اپارتمان شخصی',
    'land_government' => ' زمین دولتی ',
    'announcement' => 'اعلانات',
    'announcement_add' => 'ثبت اعلانات',
    'announce_add' => 'ثبت اعلان',
    'announcement_list' => 'لیست اعلانات',
    'category' => 'کتگوری',
    'announce_title' => 'نوع اعلان',
    'announce_content' => 'محتوا اعلان',
    'announce_category' => 'کتگوری اعلان',
    'announce_status' => 'حالت اعلان',
    'active' => 'فعال',
    'deactive' => 'غیر فعال',
    'choose_status' => 'انتخاب حالت...',
    'choose_category' => 'انتخاب کتگوری',
    'companies' => 'شرکت ها',
    'downloads' => 'داونلود ها',
    'companies_list' => 'لیست شرکت ها',
    'company_name' => 'نام شرکت',
    'companies_name' => 'نام شرکت ها',
    'license_no' => 'شماره جواز',
    'address' => 'ادرس',
    'Ans_StartDate' => 'تاریخ نشر اعلان',
    'Ans_EndDate' => 'تاریخ ختم اعلان',
    'Anss_StartDate' => 'از تاریخ نشر اعلان',
    'Anss_EndDate' => 'تا تاریخ ختم اعلان',
    'RFQ_no' => 'شماره درخواست',
    'numbers' => 'شماره',
    'datatable' => 'دیتا تیبل درخواست ها',
    'search' => 'جستجو...',
    'total_downloads' => 'تعداد داونلود ها',
    'date_base' => 'تاریخوار',









];
