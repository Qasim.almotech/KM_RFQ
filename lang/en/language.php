<?php

return [

    'Resident' => 'Resident',
    'Shahrak' => 'Shahrak',
    'type' => 'type',
    'ApartmentNo' => 'ApartmentNo',
    'Tipe' => 'Tipe',
    'Location' => 'Location',
    'Room' => 'Room',
    'Hall' => 'Hall',
    'Bathroom' => 'Bathroom',
    'kitchen' => 'kitchen',
    'Floor' => 'Floor',
    'Block' => 'Block',
    'East' => 'East',
    'West' => 'West',
    'North' => 'North',
    'South' => 'South',
    'Areasize' => 'Area-size',
    'Totalprice' => 'Total price',
    'percentage' => 'percentage',
    'NetAmount' => 'NetAmount',
    'Date' => 'Date',
    'submit' => 'submit',
    // This is related land pages
    'Land' => 'Land No',
    'District' => 'District',
    // This is related Resident page
    'Name' => 'Name',
    'FatherName' => 'Father Name',
    'GrandfatherName' => 'Grandfather Name',
    'NIC' => 'National ID Num',
    'Book' => 'Book',
    'Page' => 'Page',
    'province' => 'province',
    'Village' => 'Village',
    // This is related Paments page Apartment
    'Apartment' => 'Apartment',
    'AwizNo' => 'Awiz No',
    'PartialNo' => 'Partial No',
    'Amount' => 'Amount',
    'Land' => 'Land',
    //this is used in shahrak District text box loop
    'Districtlist' => 'list ',

    
];