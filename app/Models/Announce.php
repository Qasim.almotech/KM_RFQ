<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Announce extends Model
{
    

    use HasFactory;

    public function category()
    {
        return $this->belongsTo(AnsCategory::class);
    }

    public function company()
    {
        return $this->belongsToMany(Company::class, 'histories');
    }
}
