<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_name',
        'license_no',
        'president_name',
        'phone',
        'email',
        'address',
    ];

    public function announce()
    {
        return $this->belongsToMany(Announce::class, 'histories');
    }
}
