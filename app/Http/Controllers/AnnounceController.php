<?php

namespace App\Http\Controllers;

use App\Models\Announce;
use App\Models\AnsCategory;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;

class AnnounceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $announce_list =  Announce::all();
      
        return  view('announce.index', ['announce_list' => $announce_list ] );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = AnsCategory::all();

        return view('announce.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $request->validate([
            'rfq_no' => 'required|unique:announces',
            // 'body' => 'required',
        ]);
        $announce_add = new Announce;
        $announce_add->title = $request->title;
        $announce_add->rfq_no = $request->rfq_no;
         $announce_add->content = $request->announce_content;
         $announce_add->category_id = $request->category_id;
         $announce_add->start_date = $request->start_date;
         $announce_add->end_date = $request->end_date;
         $announce_add->status = $request->status;
         
         $ext = $request->file('data')->getClientOriginalExtension();
         
         $filename = md5(time()).".".$ext;
        // dd($request->all());
       if($ext == "pdf"){
        $request->file('data')->storeAs('public/pdf/',$filename );
       }else{
        return redirect()->route('announce.create')->with('message', 'Please select .pdf file');
       }
    //    dd($request->all());

        $announce_add->path = $filename;
        $announce_add->save();

         return redirect()->route('announce.index')->with('message', 'Announce Number '.$request->title.'  adeded succssfully');
         Toastr::success('Post added successfully :)','Success');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announce  $announce
     * @return \Illuminate\Http\Response
     */
    public function show(Announce $announce)
    {
        return view('front.show',compact('announce'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announce  $announce
     * @return \Illuminate\Http\Response
     */
    public function edit(Announce $announce)
    {
        $categories = AnsCategory::all();
        return view('announce.edit',['announce'=> $announce,"categories"=> $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announce  $announce
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announce $announce)
    {
        //
        $request->validate([
            'rfq_no' => 'required|unique:announces,rfq_no,'.$announce->id,
        ]);

        // $validated = $request->validate([
        //     'rfq_no' => 'required|unique:announces',
        //     // 'body' => 'required',
        // ]);
        $announce->title = $request->title;
        $announce->rfq_no = $request->rfq_no;
        $announce->content = $request->announce_content;
        $announce->category_id = $request->category_id;
        $announce->start_date = $request->start_date;
        $announce->end_date = $request->end_date;
        $announce->status = $request->status;
        
        if ($request->hasFile('data')) {
            $ext = $request->file('data')->getClientOriginalExtension();
        
            $filename = $request->title.".".$ext;
    
            if($ext == "pdf"){

                $request->file('data')->storeAs('public/pdf/',$filename );
            }
            else{
                return redirect()->route('announce.create')->with('message', 'Please select .pdf file');
            }

            $announce_add->path = $filename;
        }

        $announce->save();

        return redirect()->route('announce.index')->with('message', 'Announce Number '.$request->title.'  adeded succssfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announce  $announce
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announce $announce)
    {
        $announce->delete();
        return redirect()->route('announce.index')->with('message', __('general.record_deleted'));
    }
}
