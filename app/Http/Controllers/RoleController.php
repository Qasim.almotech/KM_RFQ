<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;

class RoleController extends Controller
{
      /**
       * Create a new controller instance.
       *
       * @return void
       */
      public function __construct()
      {
          // $this->middleware('guest');
          $this->middleware('auth');
      }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('role_show'))
          return view('user.access');

        $roles = Role::all();
        return view('role.create', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('role_add'))
          return view('user.access');

        $roles = Role::all();
        return view('role.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->can('role_add'))
          return view('user.access');

        $request->validate([
            "role_name" => "required",
        ]);
        $role = Role::create(['name' => $request->role_name]);
        return redirect()->route('role.index')->with('message', __('general.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!Auth::user()->can('role_show'))
          return view('user.access');

        $permissions = Permission::all();
        $data = [];

        foreach ($permissions as $key => $permission) {
          if (strpos($permission->name, 'apartment') !== FALSE) { // Yoshi version
              $data['apartment'][] = $permission;
          }
          if (strpos($permission->name, 'shahrak') !== FALSE) { // Yoshi version
              $data['shahrak'][] = $permission;
          }
          if (strpos($permission->name, 'land') !== FALSE) { // Yoshi version
              $data['land'][] = $permission;
          }
          if (strpos($permission->name, 'payment') !== FALSE) { // Yoshi version
              $data['payment'][] = $permission;
          }
          if (strpos($permission->name, 'resident') !== FALSE) { // Yoshi version
              $data['resident'][] = $permission;
          }
          if (strpos($permission->name, 'document') !== FALSE) { // Yoshi version
              $data['document'][] = $permission;
          }
          if (strpos($permission->name, 'user') !== FALSE) { // Yoshi version
              $data['user'][] = $permission;
          }
          if (strpos($permission->name, 'role') !== FALSE) { // Yoshi version
              $data['role'][] = $permission;
          }
        }

        $role = Role::find($id);
        $rolePermissions = $role->getPermissionNames()->toArray();

        return view('role.permissions', compact('data', 'id', 'rolePermissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->can('role_edit'))
          return view('user.access');

        $permissions = $request->permission_id;

        $role = Role::find($id);
        $role->syncPermissions($permissions);
        return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
