<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Lang;
use Carbon;
use App\Models\Company;

use App\Models\Announce;

use App\Models\History;

use App\Models\report;

use App\Models\AnsCategory;

use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['front']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $company = Company::all();
        $announce = Announce::all();
        $history = History::all();
        // $reports = Reports::all();
        return view('dashboard.index', compact('company', 'announce','history'));

    }

    public function countcompany()
    {
        $company = DB::table('companies')
            ->selectRaw('company_name, count(companies.id) as CompaniesID')
            //->join('apartments', 'shahraks.id', '=', 'apartments.shahrak_id')
            ->groupBy('company_name')
            ->get();
            return response()->json($company);
    }

    public function front()
    {
        
        $curentDate = Carbon\Carbon::now()->format('Y/m/d');

        $csd = explode('/', $curentDate);
        $j_y = $csd[0];
        $j_m = $csd[1];
        $j_d = $csd[2];
        $jalaliDate = gregorian_to_jalali($j_y, $j_m, $j_d);

        
        $categories = AnsCategory::all();
        
        $announces = DB::table('announces')
                        ->where('end_date', '>=', $jalaliDate)
                        ->orderBy('start_date', 'asc')
                        ->get();
        
        return view('front.index', compact('categories', 'announces'));

    }

    public function reportForm(Request $request){
        
        if(isset($request)){
            $start = faTOen($request->start_date);
            $end = faTOen($request->end_date);
            
            $announce = Announce::where('start_date', '>=', $start)
                                ->where('start_date', '<=', $end)
                                ->get();
        }else{
            $announce = Announce::all();
        }
        
        return view('reports.index', compact('announce')); //dd('form');
    }

    public function rfqReport(Request $request){

        $announce_list =  Announce::all();
        
        return view('reports.form', ['announce_list' => $announce_list ] );
    }
    public function companyList($id){
        $company = Announce::find($id)->company()->get();
        return view('reports.companylist', ['company' => $company ] );
    }
}
