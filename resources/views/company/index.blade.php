@extends('app')
@section('body')
<div class="card">
  
    <div class="card-header">
      <h3 class="card-title"></h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"><div class="dt-buttons btn-group flex-wrap"></div> </div></div>
      <div class="col-sm-12 col-md-6">
        <div id="example1_filter" class="dataTables_filter"></div></div></div>
      <div class="row">
        <div class="col-sm-12"><table id="example1" class="table table-bordered table-striped dataTable dtr-inline" aria-describedby="example1_info">
        <thead>
          <tr>
            <th>{{ __('general.companies_name') }} </th>
            <th>{{ __('general.license_no') }}</th>
            <th>{{ __('general.contact_number') }}</th>
            <th>{{ __('general.email') }}</th>
            <th>{{ __('general.address') }}</th>
            <th>{{ __('general.downloads') }}</th>
            
          </tr>
          </thead>
        <tbody>
            @foreach ($companies as $item)
            <tr>
              <td>{{ $item->company_name }}</td>
              <td>{{ $item->license_no }}</td>
              <td>{{ $item->phone }}</td>
              <td>{{ $item->email }}</td>
              <td>{{ $item->address }}</td>
              <td>{{ $item->announce->count() }}</td>
          </tr>
          @endforeach 
        </tbody> 
      </table>
    
    <!-- /.card-body -->
  </div>
  @endsection
    </div>
</div>
</div>
  @pushOnce('datatables-script')
  <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    
  <script>
    $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["excel","print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
  
  </script>
  <script>
    function confirmDelete(id) {
      let text = 'آیا مطمین استید تا این ریکارد حذف شود؟';
      if (confirm(text) == true) {
        $('form#delete'+id).submit();
    
      } else {
        alert('ریکارد حذف نشد.');
      }
    
    }
    </script>
    
    @endpushOnce