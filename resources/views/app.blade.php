<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="{{ asset('/dist/img/km-icon.png') }}">
  <title>@yield('page_title')</title>
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/toastr/toastr.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  {{-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> --}}
  <link href="{{ asset('/summernote/custom.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('/dist/css/rtl.css') }}">
  <script src="../../plugins/chart.js/Chart.min.js"></script>
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{ asset('/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
  
</head>
<body class="sidebar-mini layout-fixed">
<div class="wrapper">
    <!-- header -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    @include('layouts.header')
  </nav>
    <!-- /.header -->
    <!-- Main Sidebar Container -->
  <aside style="background-color: #001f3f;" class="main-sidebar sidebar-dark-primary elevation-4">
    @include('layouts.sidebar')
  </aside>
    <!-- Main Sidebar -->
    <!-- Main content -->
  <div class="content-wrapper" style="background-color: #e8eff8;">
    <!-- Content Header (Page header) -->
    @include('layouts.PageDetails')
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      @section('body')


      @show
    </section>
  </div>



<!-- /.content -->
  <!-- /.footer -->
  @include('layouts.footer')
  @stack('datatables-script')
  @stack('chartScript')
