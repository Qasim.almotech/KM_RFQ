@extends('app')

@section('page_title',__('general.announcement_add'))

@section('body')

<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <!-- /.card -->
          <div class="card">

            <div class="card-body">
              <form method="POST" action="{{ route('announce.store') }}" enctype="multipart/form-data">
                @method('post')
                @csrf
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label for="title">{{__('general.title')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('title') }}" type="text" class="form-control" id="title" name="title" required>
                     @error('page')
                      <span style="color: red;"> * {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="rfq_no">{{__('general.RFQ_no')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('rfq_no') }}" type="text" class="form-control" id="rfq_no" name="rfq_no" required>
                     @error('rfq_no')
                      <span style="color: red;"> * {{ $message }} </span>
                      @enderror
                    </div>
                    <br>
                    <div class="form-group">
                      <label for="role_name">{{__('general.announce_content')}}<span class="text-danger"> * </span></label>
                      <textarea id="summernote" name="announce_content" required></textarea>

                       @error('page')
                        <span style="color: red;">*  {{ $message }} </span>
                       @enderror
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="category_id">{{__('general.announce_category')}}<span class="text-danger"> * </span></label>
                      <select class="form-control" style="width: 100%;" name="category_id" required>
                        <option value="">{{__('general.choose_category')}}</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->description }}</option>
                        @endforeach
                      </select>
                      @error('page')
                          <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>{{__('general.announce_status')}}<span class="text-danger"> * </span></label>
                      <select class="form-control select2" style="width: 100%;" name="status" required>
                        <option value="">{{__('general.choose_status')}}</option>
                        <option value="active">{{__('general.active')}}</option>
                        <option value="deactive">{{__('general.deactive')}}</option>
                       
                      </select>
                      @error('type')
                          <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                     
                    
                    <div class="form-group">
                      {{-- <label for="start_date">{{__('general.start_date')}}<span class="text-danger"> * </span></label> --}}
                      <input value="{{ old('start_date') }}" type="hidden" class="form-control jalali-date"  name="start_date" readonly >
                    @error('date')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group">
                      <label for="end_date">{{__('general.Ans_EndDate')}}<span class="text-danger"> * </span></label>
                      <input value="{{ old('end_date') }}" type="text" class="form-control jalali-date"  name="end_date">
                    @error('date')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    
                    <div class="form-group">
                      <label for="exampleInputFile">انتخاب فایل<span class="text-danger"> * </span></label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" id="myfile" name="data" ><br><br>
                        </div>
                      </div>
                    </div>
                    
                    
                    <div class="row mb-0">
                      <div class="col-md-6 offset-md-4">
                          <button type="submit" class="btn btn-primary" style="text-align: center" >
                              {{ __('general.announce_add') }}
                          </button>
                      </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>

@endsection

@pushOnce('datatables-script')

<link href="{{ asset('/summernote/summernote.css') }}" rel="stylesheet">
<script src="{{ asset('/summernote/bootstrap.min.js') }}"></script>
<script src="{{ asset('/summernote/summernote.min.js') }}">
</script>

<script>
  $(document).ready(function() {
      $('#summernote').summernote({
        placeholder: 'دیتای اعلانات شاروالی کابل',
        tabsize: 2,
        height: 300
      });

  });
</script>
@endpushOnce
