@extends('app')

@section('page_title',__('general.announcement_add'))

@section('body')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <!-- /.card -->
          <div class="card">

            <div class="card-body">
              
              <form method="POST" action="{{ route('announce.update', $announce->id) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label for="title">{{__('general.announce_title')}}</label>
                      <input  type="text" class="form-control" id="title" name="title" value="{{ $announce->title }}">
                     @error('page')
                      <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="rfq_no">{{__('general.RFQ_no')}}</label>
                      <input  type="text" class="form-control" id="rfq_no" name="rfq_no" value="{{ $announce->rfq_no }}">
                     @error('rfq_no')
                      <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label for="role_name">{{__('general.announce_content')}}</label>
                      <textarea id="summernote" name="announce_content" > {{ $announce->content }} </textarea>
                    
                       @error('page')
                        <span style="color: red;">*  {{ $message }} </span>
                       @enderror
                    </div>
                    
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="category_id">{{__('general.announce_category')}}</label>
                      <select class="form-control" style="width: 100%;" name="category_id" >
                        <option value="">{{__('general.choose_category')}}</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}" @if($announce->category_id == $category->id) selected @endif >{{ $category->description }}</option>
                        @endforeach
                      </select>
                     @error('page')
                      <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <label>{{__('general.type')}}<span class="text-danger"> * </span></label>
                      <select class="form-control" style="width: 100%;" name="status">
                          <option value="">{{__('general.select')}}</option>
                          <option value="active" @if($announce->status == 'active') selected @endif>{{__('general.active')}}</option>
                          <option value="deactive" @if($announce->status == 'deactive') selected @endif>{{__('general.deactive')}} </option>
                          @error('type')
                             <span style="color: red;">*  {{ $message }} </span>
                          @enderror
                      </select>
                    </div>
                    
                    <div class="form-group">
                      <label for="start_date">{{__('general.start_date')}}<span class="text-danger"> * </span></label>
                      <input id="start_date" value="{{ $announce->start_date }}" class="form-control"  name="start_date" readonly >
                    @error('start_date')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group">
                      <label for="end_date">{{__('general.end_date')}}<span class="text-danger"> * </span></label>
                      <input type="text" id="end_date" class="form-control jalali-date"  name="end_date" value="{{ $announce->end_date }}">
                    @error('date')
                        <span style="color: red;">*  {{ $message }} </span>
                    @enderror
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">{{__('general.choose_file')}}<span class="text-danger"> * </span></label>
                      <div class="input-group">
                    <div class="custom-file">
                      <input type="file" id="myfile" name="myfile" value="{{ old('path') }}"><br><br>
                    </div>
                      </div>
                    </div>
                    <div class="row mb-0">
                      <div class="col-md-6 offset-md-4">
                          <button type="submit" class="btn btn-primary">
                              {{ __('general.announce_add') }}
                          </button>
                      </div>
                      
                  </div>
                  <br>
                 
              </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
 
@endsection

@pushOnce('datatables-script')

<link href="{{ asset('/summernote/summernote.css') }}" rel="stylesheet">
<script src="{{ asset('/summernote/bootstrap.min.js') }}"></script>
<script src="{{ asset('/summernote/summernote.min.js') }}"></script>

<script>
  $(document).ready(function() {
      $('#summernote').summernote({
        placeholder: 'دیتای اعلانات شاروالی کابل',
        tabsize: 2,
        height: 300
      });

  });

    $( document ).ready(function() {

    let start_date = {!! json_encode($announce->start_date) !!};
    let end_date = {!! json_encode($announce->end_date) !!};;
    let x = document.getElementById('end_date');
    let y = document.getElementById('start_date');
      y.value = start_date
      x.value = end_date;
    });
   
</script>
@endpushOnce
