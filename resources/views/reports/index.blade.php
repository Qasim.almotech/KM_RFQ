@extends('app')
@section('body')
<div class="card">
  
    <div class="card-header">
      <h3 class="card-title"></h3>
    </div>
  
    <!-- /.card-header -->
    <div class="card-body">
      
        <form>
        <div class="row">
        
        <div class="col-md-4">
          <div class="form-group">
            <label for="start_date">{{__('general.Anss_StartDate')}}<span class="text-danger"> * </span></label>
            <input  type="text" class="form-control jalali-date"  name="start_date">
          
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="end_date">{{__('general.Anss_EndDate')}}<span class="text-danger"> * </span></label>
            <input type="text" class="form-control jalali-date"  name="end_date">
          
          </div>
        </div>
        <div class="col-md-2">
          <br>
          <button type="submit" style="text-align: center" class="btn btn-block btn-outline-secondary btn-lg">{{__('general.search')}}</button>

        </div>
        
      </div>
    </form>
    
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
    <div class="card">
      <div class="card-header text-white" style="background-color: rgb(131, 136, 131)" >
        <h2 class="card-title">{{ __('general.datatable') }}</h2>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead style="background-color: rgb(179, 185, 179)">
          <tr>
            <th>{{ __('general.RFQ_no') }}</th>
            <th>{{ __('general.title') }}</th>
            <th>{{ __('general.announce_category') }}</th>
            <th>{{ __('general.Ans_StartDate') }}</th>
            <th>{{ __('general.Ans_EndDate') }}</th>
            <th>{{ __('general.downloads') }}</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($announce as $item)
            <tr>
              <td>{{ $item->rfq_no }}</td>
              <td>{{ $item->title }}</td>
              <td>{{ $item->category->description }}</td>
              <td>{{ $item->start_date }}</td>
              <td>{{ $item->end_date }}</td>
              <td>{{ $item->company->count() }}</td>
             </tr>
             @endforeach
          
          </tbody>
          
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</div>
<!-- /.container-fluid -->
</section>
    
    <!-- /.card-body -->
  </div>
  @endsection
    </div>
</div>
  @pushOnce('datatables-script')
  <script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    
  <script>
    $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["excel", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "dom": 'lrt',
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
  
  </script>
  <script>
    function confirmDelete(id) {
      let text = 'آیا مطمین استید تا این ریکارد حذف شود؟';
      if (confirm(text) == true) {
        $('form#delete'+id).submit();
    
      } else {
        alert('ریکارد حذف نشد.');
      }
    
    }
    </script>
    
    @endpushOnce