@extends('app')
@section('body')
<script src="{{ asset('/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
@if ($message = session('message'))
<div class=".toastrDefaultSuccess">
  <p class="text-success">
    <b><i class="fas fa-info"></i></b> {{ $message }}
  </p>
</div>
@endif
<div class="card">
  
    <div class="card-header">
      <h3 class="card-title"></h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
        <div class="row">
          <div class="col-sm-12 col-md-6"></div>
        </div>
      </div>
    
    <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" aria-describedby="example1_info">
        <thead>
          <tr>
            <th>{{ __('general.RFQ_no') }} </th>
            <th>{{ __('general.title') }} </th>
            <th>{{ __('general.category') }}</th>
            <th>{{ __('general.start_date') }}</th>
            <th>{{ __('general.total_downloads') }}</th>
            <th>{{ __('general.companies') }}</th>
          </tr>
          </thead>
        <tbody>
          @foreach ($announce_list as $item)
          <tr class="odd">
            <td>{{ $item->rfq_no }}</td>
            <td class="dtr-control sorting_1" tabindex="0"> {{ $item->title }}</td>
            <td> {{ $item->category->description }}</td>
            <td>{{ $item->start_date }}</td>
            <td>{{ $item->company->count() }}</td>
            <td>{{ $item->options }}
            
                <a class="btn btn-info btn-xs" href="{{ route('company.list', $item->id)}}">{{__('general.companies_list') }}</a>
            </td>
           
          </tr>
          @endforeach
         
        </tbody> 
      </table>

    <!-- /.card-body -->
  
  @endsection
</div>
</div>
  @pushOnce('datatables-script')
<script src="{{ asset('/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
  
<script>
  $(function () {
  $("#example1").DataTable({
    "responsive": true, "lengthChange": false, "autoWidth": false,
    "buttons": ["excel","print"]
  }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
  });
});

</script>
  
  <script>
    function confirmDelete(id) {
      let text = 'آیا مطمین استید تا این ریکارد حذف شود؟';
      if (confirm(text) == true) {
        $('form#delete'+id).submit();
    
      } else {
        alert('ریکارد حذف نشد.');
      }
    
    }
    </script>
    
    @endpushOnce