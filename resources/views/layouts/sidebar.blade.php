<!-- Main Sidebar Container -->
  <!-- Brand Logo -->


  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="  {{ asset('/dist/img/km-icon.png') }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ __('general.kabul_municipality') }}</a>
      </div>
    </div>

    <!-- SidebarSearch Form -->


    <!-- Sidebar Menu -->
    <a href="{{ route('front') }}" class="nav-link active">
      <i class="nav-icon fas fa-home"> صفحه اصلی</i>
      <p>
       
      </p>
    </a>
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item menu-open">

          @if(Route::current()->getName() == 'dashboard')
          <a href="{{ route('dashboard') }}" class="nav-link active">
            @else

          <a href="{{ route('dashboard') }}" class="nav-link">
            @endif
            <i class="nav-icon fas fa-desktop"></i>
            <p>
              {{__('general.dashboard')}}
            </p>
          </a>
        </li>

        

        <!-- Announcement -->
        
    <li class="nav-item">
      <a href="#" class="nav-link
          @if(Route::current()->getName() == 'announce.create' || Route::current()->getName() == 'announce.index')
            active
          @endif
          " >
        <i class="nav-icon fa fa-bullhorn"></i>
        <p>
        {{__('general.announcement')}}
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>

      <ul class="nav nav-treeview" style="
        @if(Route::current()->getName() == 'announce.create' || Route::current()->getName() == 'announce.index')
           display: block;
        @else
           display: none;
        @endif
      ">
        
          <li class="nav-item">
            <a href="{{ route('announce.index') }}" class="nav-link
              @if(Route::current()->getName() == 'announce.index')
                active
              @endif
            ">
              <i class="far fa-circle nav-icon"></i>
              <p>{{__('general.announcement_list')}}</p>
            </a>
          </li>
        
        
          <li class="nav-item">
            <a href="{{ route('announce.create') }}" class="nav-link
              @if(Route::current()->getName() == 'announce.create')
                active
              @endif
            ">
              <i class="nav-icon fas fa-edit"></i>
              <p>{{__('general.announcement_add')}}</p>
            </a>
          </li>
        
      </ul>
    </li>
    
<!-- End Announcement -->

<!-- Companies -->
<li class="nav-item">
  <a href="#" class="nav-link
      @if(Route::current()->getName() == 'company.index' || Route::current()->getName() == '')
        active
      @endif
      " >
    <i class="nav-icon fas fa-columns"></i>
    <p>
    {{__('general.companies')}}
      <i class="fas fa-angle-left right"></i>
    </p>
  </a>

  <ul class="nav nav-treeview" style="
    @if(Route::current()->getName() == 'company.index' || Route::current()->getName() == 'company.index')
       display: block;
    @else
       display: none;
    @endif
  ">

      <li class="nav-item">
        <a href="{{ route('company.index') }}" class="nav-link
          @if(Route::current()->getName() == 'company.index')
            active
          @endif
        ">
          <i class="far fa-circle nav-icon"></i>
          <p>{{__('general.companies_list')}}</p>
        </a>
      </li>
  </ul>
</li>
{{-- <li class="nav-item">
  <a href="{{ route('company.index') }}" class="nav-link
    @if(Route::current()->getName() == 'company.index')
      active
    @endif
  ">
    <i class="far fa-circle nav-icon"></i>
    <p>{{__('general.companies_list')}}</p>
  </a> --}}
{{-- </li>
      </a>
      <ul class="nav nav-treeview" style="
      
    ">
      
        <li class="nav-item">
          <a href="{{ route('company.index') }}" class="nav-link
            @if(Route::current()->getName() == 'company.index')
              active
            @endif
          ">
            <i class="far fa-circle nav-icon"></i>
            <p>{{__('general.companies_list')}}</p>
          </a>
        </li>
      
      </ul>
    </li> --}}
    
<!-- End Companies -->

<!-- Reports -->

    <li class="nav-item">
      <a href="#" class="nav-link
          @if(Route::current()->getName() == 'report.form' || Route::current()->getName() == 'rfq.report')
            active
          @endif
          " >
        <i class="nav-icon fa fa-newspaper"></i>
        <p>
        {{__('general.reports')}}
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>

      <ul class="nav nav-treeview" style="
        @if(Route::current()->getName() == 'report.form' || Route::current()->getName() == 'rfq.report')
           display: block;
        @else
           display: none;
        @endif
      ">

          <li class="nav-item">
            <a href="{{ route('rfq.report') }}" class="nav-link
              @if(Route::current()->getName() == 'rfq.report')
                active
              @endif
            ">
              <i class="far fa-circle nav-icon"></i>
              <p>{{__('general.RFQ_no')}}</p>
            </a>
          </li>
        
        
          <li class="nav-item">
            <a href="{{ route('report.form') }}" class="nav-link
              @if(Route::current()->getName() == 'report.form')
                active
              @endif
            ">
              <i class="nav-icon fas fa-edit"></i>
              <p>{{__('general.date_base')}}</p>
            </a>
          </li>
      </ul>
    </li>
<!-- End Reports -->



@canany(['user_add', 'user_show'])
    <li class="nav-item">
      <a href="#" class="nav-link
          @if(Route::current()->getName() == 'register' || Route::current()->getName() == 'user.index')
            active
          @endif
          " >
        <i class="nav-icon fas fa-user"></i>
        <p>
        {{__('general.users')}}
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>

      <ul class="nav nav-treeview" style="
        @if(Route::current()->getName() == 'register' || Route::current()->getName() == 'user.index')
           display: block;
        @else
           display: none;
        @endif
      ">
        @can('user_show')
          <li class="nav-item">
            <a href="{{ route('user.index') }}" class="nav-link
              @if(Route::current()->getName() == 'user.index')
                active
              @endif
            ">
              <i class="far fa-circle nav-icon"></i>
              <p>{{__('general.list_users')}}</p>
            </a>
          </li>
        @endcan
        @can('user_add')
          <li class="nav-item">
            <a href="{{ route('register') }}" class="nav-link
              @if(Route::current()->getName() == 'register')
                active
              @endif
            ">
              <i class="fa fa-user-plus nav-icon"></i>
              <p>{{__('general.add_user')}}</p>
            </a>
          </li>
        @endcan
      </ul>
    </li>
    @endcanany
    @can('role_show')
      <li class="nav-item">
        <a href="{{ route('role.create') }}"  class="nav-link">
          <i class="fa fa-gavel nav-icon"></i>
          <p>{{__('general.roles')}}</p>
        </a>
      </li>
    @endcan
    <hr/>
    <li class="nav-item">
      <form id="logout" action="{{ route('logout')}}" method="post">
        @csrf
      </form>
      <a href="javascript:{}" onclick="document.getElementById('logout').submit();" class="nav-link text-danger">
        <i class="fas fa-circle nav-icon"></i>
        <p>{{__('general.logout')}}</p>
      </a>
    </li>

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
