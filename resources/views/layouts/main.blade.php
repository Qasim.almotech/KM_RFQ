
@include("layout.header")
@include("layout.sidebar")
<div class="container">
    @yield('main-section')

</div>

@include("layout.footer")
