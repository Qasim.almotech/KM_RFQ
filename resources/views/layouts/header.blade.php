<!-- Left navbar links -->
<ul class="navbar-nav">

  <li class="nav-item d-none d-sm-inline-block">
    <img src="{{ asset('/dist/img/logo.png') }}" alt="">
  </li>

</ul>
<ul class="navbar-nav hidden">
  <li class="nav-item">
    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
  </li>


</ul>
<h3 class="main-title" >
  {{__('general.app_name')}}
</h3>
<!-- Right navbar links -->
