
@extends('app')

@section('page_title',__('general.add_role'))

@section('body')
       <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title rtl">{{__('general.add_role')}}</h3>
            </div>

            <div class="card-body">
              <form action="{{ route('role.store') }}" method="POST">
                @csrf
                <div class="row">

                    <div class="form-group col-sm-4">
                      <label for="role_name">{{__('general.role_name')}}</label>
                      <input value="{{ old('role_name') }}" type="text" class="form-control" id="role_name" name="role_name">
                     @error('page')
                      <span style="color: red;">*  {{ $message }} </span>
                      @enderror
                    </div>

                  </div>
                  <button type="submit" class="btn btn-info btn-flat">{{__('general.add_role')}}</button>
              </form>
              <hr/>
              <div class="row">
                <div class="col-md-12">
                  <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>{{__('general.role_name')}}</th>
                      <th>{{__('general.options')}}</th>

                    </tr>
                  </thead>
                  <tbody>
                    @foreach($roles as $role)
                      <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                          <a class="btn btn-info btn-xs" href="{{ route('role.show', $role->id) }}">{{__('general.permissions')}} </a>

                            <a class="btn btn-danger btn-xs" href="#">{{__('general.delete')}}</a>
                        </td>

                      </tr>
                      @endforeach
                  </tbody>
                  </table>
                </div>
              </div>
            </div>

            <!-- /.card-body -->
          </div>
        </div>
@endsection
