
@extends('app')

@section('page_title',__('general.permissions'))

@section('body')
       <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title rtl">{{__('general.permissions')}}</h3>
            </div>

            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <form action="{{ route('role.update', $id) }}" method="POST">
                    @csrf
                    @method('PUT')
                  <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>
                        {{__('general.title')}}
                      </th>
                      <th>
                        {{__('general.permissions')}}
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $key => $permissions)
                      <tr>
                        <td>{{__('general.'.$key.'s')}}</td>
                        @foreach($permissions as $permission)
                          <td>
                            <input name="permission_id[]"type="checkbox" class="role-check" value="{{ $permission->id }}" @if(in_array($permission->name, $rolePermissions)) checked @endif>
                              {{ __('role.'.$permission->name) }}
                            </td>
                        @endforeach
                      </tr>
                    @endforeach
                  </tbody>
                  </table>
                  <button type="submit" class="btn btn-info btn-flat">{{__('general.update_role')}}</button>
                </form>
                </div>
              </div>
            </div>
        </div>
@endsection
