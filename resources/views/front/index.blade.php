<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/x-icon" href="{{ asset('/dist/img/km-icon.png') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.101.0">
    <title>{{__('front.procurement_announcement')}}</title>

    <!-- Bootstrap core CSS -->

    <link rel="stylesheet" href="{{ asset('/dist/bootstrap4/css/bootstrap.min.css') }}">



    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      .main-footer  
      {
        width: 100%;
        text-align: center;
        direction: rtl;
        margin-top: 30px;
        padding-top: 30px;
        padding-bottom:30px;
        border-top: 1px solid lightgrey;
        
      }
    </style>


    <!-- Custom styles for this template -->
    <link href="{{ asset('/dist/bootstrap4/css/front.css') }}" rel="stylesheet">
  </head>
  <body>

<div class="main-menu d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-km border-bottom shadow-sm" id="main-menu">
  <h5 class="my-0 mr-md-auto font-weight-normal"><img src="{{ asset('dist/img/logo.png') }}"></h5>
  <nav class="my-2 my-md-0 mr-md-3 top-menu">
    <a class="p-2 text-dark" href="/">صفحه اصلی</a>
    <a class="p-2 text-dark" href="#">اعلانات</a>
    <a class="p-2 text-dark" href="#">درباره ما</a>
    <a class="p-2 text-dark" href="#">تماس با ما</a>
  </nav>
  <!-- <a class="btn btn-outline-light" href="#">Sign up</a> -->
  
</div>

<div class="container-fluid">
  <div class="row coll-data">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
    <div class=" px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">
        {{__('front.procurement_announcement')}}
      </h1>
      <p class="lead">
        {{__('front.procurement_description')}}
      </p>
    </div>

      <div class="accordion" id="accordionExample">
        @foreach($categories as $category)
          
            
            <div class="card">
              <div class="card-header" id="heading{{$category->id}}">
                  <a class="btn btn-block text-right collapsed" type="button" data-toggle="collapse" data-target="#collapse{{$category->id}}" aria-expanded="true" aria-controls="collapse{{$category->id}}">
                    <h5 class="mb-0">{{$category->id}} - {{ $category->description }}  </h5>
                  </a>
              </div>

              <div id="collapse{{$category->id}}" class="collapse" aria-labelledby="heading{{$category->id}}" data-parent="#accordionExample">
                <div class="card-body">
                  <table class="table table-bordered">
                    <tr>
                      <th>
                        {{ __('general.title') }}
                      </th>
                      <th>
                        {{ __('general.Ans_StartDate') }}
                      </th>
                      <th>
                        {{ __('general.Ans_EndDate') }}
                      </th>
                      <th>
                        {{ __('general.options') }}
                      </th>
                    </tr>
                  @foreach($announces as $announce)
                    @if($announce->category_id == $category->id)
                      <tr>
                        <td>
                          {{ $announce->title }}
                        </td>
                        <td>
                          {{ $announce->start_date }}
                        </td>
                        <td>
                          {{ $announce->end_date }}
                        </td>
                        <td>
                          <a class="btn btn-primary" href="{{ route('announce.show', $announce->id) }}">{{__('front.apply')}}</a>
                        </td>
                      </tr>
                    @endif
                  @endforeach
                  </table>
                </div>
              </div>
            </div>

          
        @endforeach

      </div>
    </div>
    <div class="col-md-2">
    </div>
  </div>
</div>


@include('layouts.footer')
</body>
</html>
