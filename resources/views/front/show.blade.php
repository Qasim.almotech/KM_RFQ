<!doctype html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <link rel="icon" type="image/x-icon" href="{{ asset('/dist/img/km-icon.png') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.101.0">
    <title>{{__('front.procurement_announcement')}}</title>

    <!-- Bootstrap core CSS -->

    <link rel="stylesheet" href="{{ asset('/dist/bootstrap4/css/bootstrap.min.css') }}">



    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      .main-footer  
      {
        width: 100%;
        text-align: center;
        direction: rtl;
        margin-top: 30px;
        padding-top: 30px;
        padding-bottom:30px;
        border-top: 1px solid lightgrey;
        
      }
    </style>


    <!-- Custom styles for this template -->
    <link href="{{ asset('/dist/bootstrap4/css/front.css') }}" rel="stylesheet">
  </head>
  <body>

<div class="main-menu d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-km border-bottom shadow-sm" id="main-menu">
  <h5 class="my-0 mr-md-auto font-weight-normal"><img src="{{ asset('dist/img/logo.png') }}"></h5>
  <nav class="my-2 my-md-0 mr-md-3 top-menu">
    <a class="p-2 text-dark" href="/">صفحه اصلی</a>
    <a class="p-2 text-dark" href="/">اعلانات</a>
    <a class="p-2 text-dark" href="#">درباره ما</a>
    <a class="p-2 text-dark" href="#">تماس با ما</a>
  </nav>
  <!-- <a class="btn btn-outline-light" href="#">Sign up</a> -->
  
</div>
<div class="container">
  <div class="row coll-data">
    <div class="col-md-7">
      @error('company_name')
          <span style="color: red;">*  {{ $message }} </span><br/>
      @enderror
      @error('license_no')
          <span style="color: red;">*  {{ $message }} </span><br/>
      @enderror
      @error('phone')
          <span style="color: red;">*  {{ $message }} </span><br/>
      @enderror
      @error('email')
          <span style="color: red;">*  {{ $message }} </span><br/>
      @enderror
      @error('address')
          <span style="color: red;">*  {{ $message }} </span><br/>
      @enderror
        <div class="card">
            <div class="card-body">
              <table class="table table-bordered">
                <tr>
                  <th>{{__('general.title')}}</th>
                  <td colspan="3">
                    <h5 class="card-title">{{ $announce->title }}
                  </td>
                </tr>
                
              </table>
                {!! $announce->content !!}
            </div>
        </div>
        
    </div>

    <div class="col-md-5">
         <table class="table table-bordered">
            <tr>
              <th>
                {{__('general.category')}}
              </th>
              <td>
                {{ $announce->category->description }}
              </td>
            </tr>
            <tr>
              <th>
                {{__('general.RFQ_no')}}
              </th>
              <td>
                {{ $announce->rfq_no }}
              </td>
            </tr>
            <tr>
              <th>
                {{__('general.Ans_StartDate')}}
              </th>
              <td>
                {{ $announce->start_date }}
              </td>
            </tr>
            <tr>
              <th>
                {{__('general.Ans_EndDate')}}
              </th>
              <td>
                {{ $announce->end_date }}
              </td>
            </tr>
            <tr>
              <td colspan="2">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-download" viewBox="0 0 16 16">
                <path d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                <path d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
              </svg>

              {{__('front.download')}}

              </button>
              </td>
            </tr>
          </table>      
    </div>
    
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">{{__('front.register_company')}}</h5>
        
      </div>
      <div class="modal-body">
        
      <form method="POST" action="{{ route('company.store') }}" enctype="multipart/form-data" id="company-form">
        @csrf
        <input type="hidden" name="announce_id" value="{{ $announce->id }}">
        
        <div class="form-group text-right">
          <label for="company_name">{{__('front.company_name')}}</label>
          <input type="text" class="form-control" id="company_name" name="company_name" required>
          
        </div>
        <div class="form-group text-right">
          <label for="license_no">{{__('front.license_no')}}</label>
          <input type="text" class="form-control" id="license_no" name="license_no" required>
          
        </div>
        <div class="form-group text-right">
          <label for="phone">{{__('front.phone')}}</label>
          <input type="text" class="form-control" id="phone" name="phone" required>
        </div>
        <div class="form-group text-right">
          <label for="email">{{__('front.email')}}</label>
          <input type="email" class="form-control" id="email" name="email" required>
        </div>
        <div class="form-group text-right">
          <label for="address">{{__('front.address')}}</label>
          
          <input type="text" class="form-control" id="address" name="address" re>
          
          <input type="hidden" class="form-control" id="address" name="download_link" value="{{$announce->path }}">
        </div>
        
        <button  type="submit" class="btn btn-primary" id="dl-btn">{{__('front.save_and_download')}}</button>
      </form>

      </div>
      <div class="modal-footer">
        <p class="text-center">
          {{__('front.company_footer_message')}}
        </p>
      </div>
    </div>
  </div>
</div>

@include('layouts.footer')
</body>

{{-- <script>
  $('#dl-btn').click(function() {
    $("#company-form").submit();
    
    $('#exampleModal').removeClass("show");
    // location.reload();
  });

</script> --}}
</html>
